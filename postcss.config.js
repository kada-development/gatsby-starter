module.exports = {
  plugins: [
    require('postcss-import'),
    require('postcss-preset-env')({
      features: {
        customProperties: false,
      }
    }),
    require('postcss-css-variables')({
      // preserve: true,
    }),
    require('cssnano')({
      preset: 'advanced',
      autoprefixer: false,
    }),
  ],
};
