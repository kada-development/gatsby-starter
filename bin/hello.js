const pkg = require('../package.json')

if (pkg.initialized) {
  return
}

const chalk = require('chalk')
const fs = require('fs').promises

const inquirer = require('inquirer')

const hello = async () => {
  console.log(`

  Hello! 👋
  We will ask some questions to finish setup
  `)
  let {
    firebase,
    title,
    site_url,
    name,
    description,
    google_analytics,
    manifest_name,
    manifest_short_name,
  } = await inquirer.prompt([
    {
      type: 'input',
      name: 'firebase',
      message: 'What is your firebase project ID?',
    },
    {
      type: 'input',
      name: 'title',
      message: 'What is the title of this site?',
    },
    {
      type: 'input',
      name: 'site_url',
      message: 'What is the URL of the site?',
    },
    {
      type: 'input',
      name: 'name',
      message: 'What is the name that should be in package.json?',
    },
    {
      type: 'input',
      name: 'description',
      message: 'What is the description for package.json?',
    },
    {
      type: 'input',
      name: 'google_analytics',
      message: 'What is your google tag ID thingy?',
    },
    {
      type: 'input',
      name: 'manifest_name',
      message: 'What is the manifest name?',
    },
    {
      type: 'input',
      name: 'manifest_short_name',
      message: 'What is the manifest SHORT name?',
    },
  ])

  if (site_url && site_url.indexOf('http') !== 0) {
    site_url = 'https://' + site_url
  }

  await replaceInFile('./.firebaserc', /__FIREBASE_PROJECT_NAME__/g, firebase)
  await replaceInFile('./gatsby-config.js', /__SITE_TITLE__/g, title)
  await replaceInFile('./.gitlab-ci.yml', /__SITE_URL__/g, site_url)
  await replaceInFile('./package.json', /__NAME__/g, name)
  await replaceInFile('./package.json', /__DESCRIPTION__/g, description)
  await replaceInFile('./gatsby-config.js', /__GOOGLE_ANALYTICS__/g, google_analytics)
  await replaceInFile('./gatsby-config.js', /__MANIFEST_NAME__/g, manifest_name)
  await replaceInFile('./gatsby-config.js', /__MANIFEST_SHORT_NAME__/g, manifest_short_name)

  const pkg = JSON.parse(await fs.readFile('./package.json', 'utf8'))
  pkg.initialized = true
  await fs.writeFile('./package.json', JSON.stringify(pkg, undefined, 2))

  console.log(`Done! Make sure to check out ${chalk.green(
    'gatsby-config.js',
  )} and the manifest section.
  Also take a look at ${chalk.green('src/colors.json')} in order to specify your theme colors`)
}

const replaceInFile = async (filename, regex, replacement) => {
  if (!replacement) {
    return
  }
  const data = await fs.readFile(filename, 'utf8')
  const result = data.replace(regex, replacement)
  await fs.writeFile(filename, result)
}

hello()
