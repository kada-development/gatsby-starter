import * as React from 'react'
import Helmet from 'react-helmet'

import Header from '../components/header'
import './index.css'
import { Site } from '../graphql-types'

interface DefaultLayoutProps {
  data: {
    site: Site
  }
  location: {
    pathname: string
  }
  children: any
}
export class DefaultLayout extends React.PureComponent<DefaultLayoutProps, void> {
  render() {
    return (
      <div>
        <Helmet
          title={this.props.data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: 'Sample' },
            { name: 'keywords', content: 'sample, something' },
          ]}
        />
        <Header siteTitle={this.props.data.site.siteMetadata.title} />
        <div
          style={{
            margin: '0 auto',
            maxWidth: 960,
            padding: '0px 1.0875rem 1.45rem',
            paddingTop: 0,
          }}
        >
          {this.props.children()}
        </div>
      </div>
    )
  }
}

export default DefaultLayout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
