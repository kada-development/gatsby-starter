import * as React from 'react'
import Link from 'gatsby-link'

import * as colors from '../colors.json'

const Header = ({ siteTitle }) => (
  <div style={{ background: colors.primary, marginBottom: '1.45rem' }}>
    <div style={{ margin: '0 auto', maxWidth: 960, padding: '1.45rem 1.0875rem' }}>
      <h1 style={{ margin: 0 }}>
        <Link to="/" style={{ color: 'white', textDecoration: 'none' }}>
          {siteTitle}
        </Link>
      </h1>
    </div>
  </div>
)
export default Header
