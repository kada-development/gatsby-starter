declare const graphql: (query: TemplateStringsArray) => void

declare module '*.png'
declare module '*.scss'
declare module '*.jpg'
declare module '*.jpeg'
declare module '*.svg'
declare module '*.mp4'
declare module '*.webm'
declare module '*.webp'
